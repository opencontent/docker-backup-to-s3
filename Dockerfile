FROM debian:12.6

RUN apt-get update && \
    apt-get install -y python-is-python3 cron s3cmd && \
    rm -rf /var/lib/apt/lists/*

COPY s3cfg /root/.s3cfg

COPY start.sh /start.sh
RUN chmod +x /start.sh

COPY sync.sh /sync.sh
RUN chmod +x /sync.sh

COPY get.sh /get.sh
RUN chmod +x /get.sh

ENTRYPOINT ["/start.sh"]
CMD [""]
